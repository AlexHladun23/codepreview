import {
  createSlice,
  createEntityAdapter,
  createAsyncThunk,
  SerializedError,
  createSelector,
  createAction,
} from '@reduxjs/toolkit'
import { AvailableTestType } from 'redux/availableTests/types'
import { instance } from '../../api/axios.config'
import { RootState } from 'redux/store'
import { TQuestion } from './types'
import { QuestionType } from 'redux/availableTests/types'
import { ResponseQuestionTypes } from 'redux/testCreating/types'
import { AxiosError } from 'axios'

/*** THUNKS ***
 **************/
const getTestForPassing = createAsyncThunk(
  'test/forPassing',
  async (id: number, { rejectWithValue }) => {
    try {
      const { data } = await instance.get<AvailableTestType>(`/test/${id}/passing`)
      return data
    } catch (err) {
      let error: AxiosError = err
      if (error.response?.data.statusCode === 403) {
        return rejectWithValue({ message: 'Access denied' })
      }
      throw err
    }
  }
)

const saveTestAnswers = createAsyncThunk(
  'test/sendAnswers',
  async (_, { getState }: { getState: () => RootState }) => {
    const result = {
      testId: getState().passTests.test.id,
      questions: getState().passTests.result.map((item: TQuestion) => {
        if (item.openAnswer || item.openAnswer === '') {
          return { id: item.id, openAnswer: item.isAnswered ? item.openAnswer : '' }
        } else if (item.answers) {
          return {
            id: item.id,
            answers: item.isAnswered
              ? item.answers
              : item.answers.map(answer => {
                  return { ...answer, isCorrect: false }
                }),
          }
        }
      }),
    }
    const { data } = await instance.post<boolean>(
      `/result/${getState().passTests.test.passId}/save`,
      result
    )
    return data
  }
)

/*** ACTIONS ***
 ***************/
const answer = createAction(
  'test/pass/question/answers',
  (id: number, isOpen: boolean, questionId: number, answerIds?: number[], openAnswer?: string) => ({
    payload: {
      id,
      answerIds,
      openAnswer,
      isOpen,
      questionId,
    },
  })
)
const setPassing = createAction('passTests/setPassing', (isPassing: boolean) => ({
  payload: isPassing,
}))

const clearOldResult = createAction('passTests/clearOldResult')

const setIsAnswered = createAction(
  'passTests/setIsAnswered',
  (questionId: number, isAnswered: boolean) => ({
    payload: { questionId, isAnswered },
  })
)

const clearNoMoreTries = createAction('passTests/clearNoMoreTries')

const adapter = createEntityAdapter<AvailableTestType>()

const initialState = adapter.getInitialState({
  isLoading: false,
  test: null as AvailableTestType | null,
  error: null as SerializedError | null,
  result: null as TQuestion[] | null,
  isPassing: false,
  noMoreTries: false,
  accessDenied: false,
})

export const passTestsReducer = createSlice({
  name: 'tests/pass',
  initialState,
  reducers: {},
  extraReducers: ({ addCase }) => {
    /*** getTestForPassing ***
     *************************/
    addCase(getTestForPassing.pending, state => {
      state.isLoading = true
      state.noMoreTries = false
      state.error = null
    })
    addCase(getTestForPassing.fulfilled, (state, { payload }) => {
      state.noMoreTries = false
      state.isLoading = false
      state.error = null
      state.test = payload

      state.result = payload.questions.map((question: QuestionType) => {
        if (question.type === ResponseQuestionTypes.FullAnswer) {
          return {
            id: question.id,
            openAnswer: '',
            isAnswered: false,
          }
        } else {
          return {
            id: question.id,
            answers: question.answers.map(({ id, title }) => {
              return { id, title, isCorrect: false }
            }),
            isAnswered: false,
          }
        }
      })
      state.isPassing = true
    })
    addCase(getTestForPassing.rejected, (state, action) => {
      if (action.payload) {
        state.accessDenied = true
      } else {
        state.noMoreTries = true
      }
      state.isLoading = false
      state.error = action.error
    })

    /*** saveTestAnswers ***
     ***********************/
    addCase(saveTestAnswers.pending, state => {
      state.isLoading = true
      state.error = null
    })
    addCase(saveTestAnswers.fulfilled, state => {
      state.isLoading = false
      state.error = null
    })
    addCase(saveTestAnswers.rejected, (state, { error }) => {
      state.isLoading = false
      state.error = error
    })

    /*** answer ***
     **************/
    addCase(answer, (state, { payload }) => {
      const currentQuestion = state.result?.find(item => item.id === payload.questionId)
      if (currentQuestion) {
        if (payload.isOpen && payload.openAnswer) {
          currentQuestion.openAnswer = payload.openAnswer
        } else {
          currentQuestion?.answers?.map(item => {
            if (payload.answerIds?.find(answer => answer === item.id)) {
              item.isCorrect = true
            } else {
              item.isCorrect = false
            }
          })
        }
      }
    })
    addCase(setPassing, (state, action) => {
      state.isPassing = action.payload
    })
    addCase(clearOldResult, (state, action) => {
      state.result = null
    })
    addCase(setIsAnswered, (state, { payload }) => {
      const currentQuestion = state.result?.find(question => question.id === payload.questionId)
      if (currentQuestion) currentQuestion.isAnswered = payload.isAnswered
    })
    addCase(clearNoMoreTries, state => {
      state.noMoreTries = false
      state.accessDenied = false
    })
  },
}).reducer

const rootSelect = (state: RootState) => state.passTests

export const passTestsLogics = {
  actions: {
    getTestForPassing,
    answer,
    saveTestAnswers,
    setPassing,
    clearOldResult,
    setIsAnswered,
    clearNoMoreTries,
  },
  selectors: {
    test: createSelector(rootSelect, (state: typeof initialState) => state.test),
    error: createSelector(rootSelect, (state: typeof initialState) => state.error),
    isLoading: createSelector(rootSelect, (state: typeof initialState) => state.isLoading),
    result: createSelector(rootSelect, (state: typeof initialState) => state.result),
    isPassing: createSelector(rootSelect, (state: typeof initialState) => state.isPassing),
    noMoreTries: createSelector(rootSelect, (state: typeof initialState) => state.noMoreTries),
    accessDenied: createSelector(rootSelect, (state: typeof initialState) => state.accessDenied),
  },
}
